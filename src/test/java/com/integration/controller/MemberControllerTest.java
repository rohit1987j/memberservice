package com.integration.controller;

import com.domain.Member;
import com.dto.AddMemberRequest;
import com.exceptionHandler.ExceptionResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MemberControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void addShouldReturn400HttpStatusCodeWhenFirstNameIsNull() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                null,
                "lastName",
                12345,
                            new Date());
        ResponseEntity responseEntity = restTemplate.postForEntity("/members", addMemberRequest, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void addShouldReturn400HttpStatusCodeWhenFirstNameIsEmpty() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "",
                "lastName",
                12345,
                new Date());
        ResponseEntity responseEntity = restTemplate.postForEntity("/members", addMemberRequest, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void addShouldReturn400HttpStatusCodeWhenLastNameIsNull() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "firstName",
                null,
                12345,
                new Date());
        ResponseEntity responseEntity = restTemplate.postForEntity("/members", addMemberRequest, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void addShouldReturn400HttpStatusCodeWhenLastNameIsEmpty() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "firstName",
                "",
                12345,
                new Date());
        ResponseEntity responseEntity = restTemplate.postForEntity("/members", addMemberRequest, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void addShouldReturn400HttpStatusCodeWhenPostalCodeIsLessThen5Digits() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "firstName",
                "lastName",
                1234,
                new Date());
        ResponseEntity responseEntity = restTemplate.postForEntity("/members", addMemberRequest, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }

    @Test
    public void addShouldReturn201HttpStatusWhenRequestDataIsValid() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "firstName",
                "lastName",
                12345678,
                new Date());
        ResponseEntity responseEntity = restTemplate.postForEntity("/members", addMemberRequest, null);
        Assert.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    public void updateShouldReturn400HttpStatusCodeWhenMemberIdIsNotValid() {
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "firstName",
                "lastName",
                12345678,
                new Date());

        HttpEntity<AddMemberRequest> entity = new HttpEntity<AddMemberRequest>(addMemberRequest);
        ResponseEntity responseEntity = restTemplate.exchange("/members/100", HttpMethod.PUT, entity, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void getShouldReturn400HttpStatusCodeWhenMemberIdIsNotValid() {
        ResponseEntity responseEntity = restTemplate.getForEntity("/members/100",ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void getShouldReturn200HttpStatusCodeWhenMemberIdIsValid() {
        addShouldReturn201HttpStatusWhenRequestDataIsValid();

        ResponseEntity responseEntity = restTemplate.getForEntity("/members/1", Member.class);
        Member member = (Member) responseEntity.getBody();

        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assert.assertNotNull(member);
    }

    @Test
    public void deleteShouldReturn400HttpStatusCodeWhenMemberIdIsNotValid() {
        ResponseEntity responseEntity = restTemplate.exchange("/members/100", HttpMethod.DELETE, null, ExceptionResponse.class);
        Assert.assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void deleteShouldReturn201HttpStatusCodeWhenMemberIdIsValid() {
        addShouldReturn201HttpStatusWhenRequestDataIsValid();
        getMembersShouldReturn200HttpStatusCode();
        ResponseEntity responseEntity = restTemplate.exchange("/members/1", HttpMethod.DELETE, null, ResponseEntity.class);
        getShouldReturn400HttpStatusCodeWhenMemberIdIsNotValid();
    }

    @Test
    public void getMembersShouldReturn200HttpStatusCode() {
        addShouldReturn201HttpStatusWhenRequestDataIsValid();
        addShouldReturn201HttpStatusWhenRequestDataIsValid();

        ResponseEntity responseEntity = restTemplate.getForEntity("/members",null);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
