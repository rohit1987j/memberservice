package com.unit.service;

import com.domain.Member;
import com.repository.MemberRepository;
import com.service.MemberService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MemberServiceTest {

    private MemberService memberService;

    @Mock
    private MemberRepository memberRepository;

    @Before
    public void init() {
        memberService = new MemberService(memberRepository);
    }

    @Test
    public void getShouldReturnMemberWithGivenId() {
        Member member = new Member("firstName", "lastName",
                123, new java.sql.Date(new Date().getTime()));

        when(memberRepository.findOne(any(Integer.class))).thenReturn(member);

        final Member addedMember = memberService.get(1);

        assertEquals(member, addedMember);
    }

    @Test
    public void getAllShouldReturnAllMembers() {
        List<Member> members = createMemberList();

        when(memberRepository.findAll()).thenReturn(members);

        List<Member> list = memberService.getAll();
        assertEquals(members.size(), list.size());
    }

    private List<Member> createMemberList() {
        Member member1 = new Member("firstName1", "lastName1",
                123, new java.sql.Date(new Date().getTime()));
        Member member2 = new Member("firstName2", "lastName2",
                123, new java.sql.Date(new Date().getTime()));

        ArrayList<Member> members = new ArrayList<>();
        members.add(member1);
        members.add(member2);

        return members;
    }
}
