package com.unit.controller;

import com.controller.MemberController;
import com.domain.Member;
import com.dto.AddMemberRequest;
import com.exceptionHandler.ExceptionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.MemberService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Date;

import static com.utilities.Errors.ErrorCode.VALIDATION_ERROR_CODE;
import static com.utilities.Errors.ErrorMessage.VALIDATION_ERROR_MESSAGE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@WebMvcTest(MemberController.class)
public class MemberControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private MemberService memberService;

    @Test
    public void addMemberShouldReturnBadRequestWhenFirstNameIsNotPresent() throws Exception {
        doNothing().when(memberService).add(any(AddMemberRequest.class));

        AddMemberRequest addMemberRequest = new AddMemberRequest(
                null,
                "Jain",
                12334,
                new Date()
        );

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/members").contentType(MediaType.APPLICATION_JSON).content(
                objectMapper.writeValueAsString(addMemberRequest)).accept(MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.BAD_REQUEST.value(), result.getResponse().getStatus());

        ExceptionResponse exceptionResponse = objectMapper.readValue(result.getResponse().getContentAsString(), ExceptionResponse.class);
        assertEquals(VALIDATION_ERROR_CODE, exceptionResponse.getErrorCode());
        assertEquals(VALIDATION_ERROR_MESSAGE, exceptionResponse.getErrorMessage());
    }

    @Test
    public void addMemberShouldAddMemberWhenJsonRequestIsSent() throws Exception {
        doNothing().when(memberService).add(any(AddMemberRequest.class));
        AddMemberRequest addMemberRequest = new AddMemberRequest(
                "firstName",
                "lastName",
                12334,
                new Date()
        );

        ObjectMapper objectMapper = new ObjectMapper();
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/members").contentType(MediaType.APPLICATION_JSON).content(
                        objectMapper.writeValueAsString(addMemberRequest)).accept(MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());
    }

    @Test
    public void addMemberShouldAddMemberWhenXMLRequestIsSent() throws Exception {
        doNothing().when(memberService).add(any(AddMemberRequest.class));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/members").contentType(MediaType.APPLICATION_XML).content(
                getValidMemberAsXml()).accept(MediaType.APPLICATION_XML);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());
    }

    @Test
    public void getMemberShouldReturn200StatusWhenUserIsPresent() throws Exception {
        Mockito.when(memberService.get(any(Integer.class))).thenReturn(
                new Member("firstName", "lastName",
                        12345, new java.sql.Date(new Date().getTime())));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/members/1").accept(
                        MediaType.APPLICATION_JSON);
        MvcResult result = mvc.perform(requestBuilder).andReturn();
        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
    }

    @Test
    public void deleteMemberShouldDeleteMemberWhenUserIsPresentAndReturn204() throws Exception {
        Mockito.when(memberService.get(any(Integer.class))).thenReturn(mock(Member.class));
        doNothing().when(memberService).delete(any(Integer.class));

        int memberId = 1;
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(
                "/members/" + memberId).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mvc.perform(requestBuilder).andReturn();

        assertEquals(HttpStatus.NO_CONTENT.value(), result.getResponse().getStatus());
    }

    public String getValidMemberAsXml() {
        return "<member><firstName>Rohit</firstName><lastName>Jain</lastName>" +
                "<postalCode>12312</postalCode><dob>1987-08-08</dob></member>";
    }

}