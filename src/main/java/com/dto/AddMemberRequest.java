package com.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class AddMemberRequest {

    @Size(min=1, max=20)
    @NotNull(message = "firstName cannot be null or empty")
    private String firstName;

    @Size(min=1, max=20)
    @NotNull(message = "lastName cannot be null or empty")
    private String lastName;

    @Min(10000)
    @NotNull(message = "postalCode cannot be empty")
    private Integer postalCode;

    @Past
    @NotNull(message = "dob cannot be empty")
    private Date dob;
}
