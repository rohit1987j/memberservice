package com.dto;

import com.domain.Member;

import java.util.List;

public class Members {
    List<Member> member;

    public List<Member> getMembers() {
        return member;
    }

    public void setMembers(List<Member> member) {
        this.member = member;
    }
}
