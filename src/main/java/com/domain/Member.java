package com.domain;

import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Getter
@Table(name = "member")
public class Member {

    @Id
    @GeneratedValue
    private int id;

    private String firstName;

    private String lastName;

    private Integer postalCode;

    private Date dob;

    public Member() {

    }

    public Member(final String firstName, final String lastName, final Integer postalCode, final Date dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
        this.dob = dob;
    }

    public Member(final int id, final String firstName, final String lastName, final Integer postalCode, final Date dob) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.postalCode = postalCode;
        this.dob = dob;
    }
}
