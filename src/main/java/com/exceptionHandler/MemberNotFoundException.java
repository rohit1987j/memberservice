package com.exceptionHandler;

import lombok.ToString;

@ToString
public class MemberNotFoundException extends RuntimeException {

    public MemberNotFoundException(final long memberId) {
        super(String.format("No member found for id : %s", memberId));
    }
}
