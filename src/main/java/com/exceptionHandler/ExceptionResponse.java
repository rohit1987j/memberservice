package com.exceptionHandler;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class ExceptionResponse {

    private String errorCode;

    private String errorMessage;

    private List<String> errors;
}