package com.utilities;

public class Errors {

    public static final class ErrorCode {
        public static final String VALIDATION_ERROR_CODE = "Validation errors";
        public static final String MEMBER_NOT_FOUND = "Not found.";
    }

    public static final class ErrorMessage {
        public static final String VALIDATION_ERROR_MESSAGE = "Invalid input.";
    }
}
