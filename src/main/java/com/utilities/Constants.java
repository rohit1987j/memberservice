package com.utilities;

public class Constants {

    public static final class API_URL {
        public static final String MEMBERS = "/members";
    }

    public static final class ERRORS {

        public static final String COUNT_CANNOT_BE_LESS_THEN_ZERO = "430";
        public static final String TIMESTAMP_CANNOT_BE_NULL = "431";
        public static final String TIMESTAMP_CANNOT_BE_OLDER_THEN_60_SECONDS = "432";
    }
}
