package com.service;

import com.domain.Member;
import com.dto.AddMemberRequest;
import com.exceptionHandler.MemberNotFoundException;
import com.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class MemberService {

    private MemberRepository memberRepository;

    @Autowired
    public MemberService(final MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @Transactional
    public void add(final AddMemberRequest addMemberRequest) {
        final Member member = new Member(
                addMemberRequest.getFirstName(),
                addMemberRequest.getLastName(),
                addMemberRequest.getPostalCode(),
                new java.sql.Date(addMemberRequest.getDob().getTime()));
        memberRepository.save(member);
    }

    @Transactional
    public void update(final int memberId, final AddMemberRequest addMemberRequest) {
        if (memberRepository.exists(memberId)) {
            final Member member = new Member(
                    memberId,
                    addMemberRequest.getFirstName(),
                    addMemberRequest.getLastName(),
                    addMemberRequest.getPostalCode(),
                    new java.sql.Date(addMemberRequest.getDob().getTime()));
            memberRepository.save(member);
        } else {
            throw new MemberNotFoundException(memberId);
        }

    }

    @Transactional
    public Member get(final int memberId) {
        Member member = memberRepository.findOne(memberId);
        if(Objects.isNull(member)) {
            throw new MemberNotFoundException(memberId);
        }
        return member;
    }

    @Transactional
    public void delete(final int memberId) {
        if (memberRepository.exists(memberId)) {
            memberRepository.delete(memberId);
        } else {
            throw new MemberNotFoundException(memberId);
        }
    }

    @Transactional
    public List<Member> getAll() {
        Iterable<Member> members = memberRepository.findAll();
        List<Member> list = new ArrayList<>();
        members.forEach(member -> list.add(member));
        return list;
    }
}
