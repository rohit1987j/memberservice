package com.controller;

import com.domain.Member;
import com.dto.AddMemberRequest;
import com.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/members", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
public class MemberController {

    @Autowired
    private MemberService memberService;

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity addMember(@Valid @RequestBody AddMemberRequest addMemberRequest) {
        memberService.add(addMemberRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity getMember(@PathVariable("id") Integer id) {
        Member member = memberService.get(id);
        return ResponseEntity.ok(member);
    }

    @PutMapping(value = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity updateMember(@Valid @RequestBody AddMemberRequest addMemberRequest,
                                       @PathVariable("id") Integer id) {
        memberService.update(id, addMemberRequest);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping
    public ResponseEntity getMembers() {
        Iterable<Member> members = memberService.getAll();
        return ResponseEntity.ok(members);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity deleteMember(@PathVariable("id") Integer id) {
        memberService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
